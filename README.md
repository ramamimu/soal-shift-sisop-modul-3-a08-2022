# Soal 1 : Base64 Decoding

## a. Unzip file music.txt dan quote.txt

Pertama disiapkan array untuk menampung thread.

```c
pthread_t tid[15];
```

Selanjutnya membuat directory modul dengan process menggunakan `fork()` dan `execv()`.

```c
    // Membuat directory modul
    pid_t child = fork();
    if (child < 0)
        exit(EXIT_FAILURE);

    else if (child == 0) {
        char *argv[] = {"mkdir", "modul", NULL};
        execv("/bin/mkdir", argv);
    }
    else {
        printf("\n-- membuat directory modul berhasil\n");
        wait(0);
    }
```

Untuk dapat meng-unzip file bersamaan menggunakan thread perlu dibuat fungsi thread sebagai berikut.

```c
void *unzipQuote(void *arg);
void *unzipMusic(void *arg);
```

Didalam fungsi thread tersebut dilakukan process unzip file menggunakan `fork()` dan `execl()`.

```c
void *unzipQuote(void *arg){
    pid_t child = fork();
    if (child < 0)
        exit(EXIT_FAILURE);

    else if (child == 0) {
        // unzip quote.zip
        // file quote.zip disiapkan
        execl("/bin/unzip", "unzipQuote", "/home/ariefbadrus/Downloads/quote.zip", "-d", "/home/ariefbadrus/soal-shift-sisop-modul-3-a08-2022/soal1/modul/quote", NULL);
    }
    else
        wait(0);
    printf("\n-- unzip quote berhasil\n");
}

void *unzipMusic(void *arg){
    pid_t child = fork();
    if (child< 0)
        exit(EXIT_FAILURE);

    else if (child == 0) {
        // unzip music.zip
        // file music.zip disiapkan
        execl("/bin/unzip", "unzipMusic", "/home/ariefbadrus/Downloads/music.zip", "-d", "/home/ariefbadrus/soal-shift-sisop-modul-3-a08-2022/soal1/modul/music", NULL);
    }
    else
        wait(0);
    printf("\n-- unzip music berhasil\n");
}
```

Thread dibuat dan dijalankan bersama-sama menggunakan perintah berikut.

```c
    // Unzip music.zip & quote.zip menggunakan thread
    pthread_create(&(tid[0]), NULL, &unzipQuote, NULL);
    pthread_create(&(tid[1]), NULL, &unzipMusic, NULL);
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);
```

## b. Decode file .txt dengan base64

Pertama perlu disiapkan fungsi-fungsi utilitas yang diperlukan untuk decode dengan base64. Fungsi-fungsi dibawah kami peroleh dari website (link: https://nachtimwald.com/2017/11/18/base64-encode-and-decode-in-c/).

```c
int b64_decodeTable[];
size_t b64_decodedSize(const char *inputString);
int b64_isValidChar(char c);
int b64_decode(const char *inputString, unsigned char *decodedString, size_t outLength);
```

Untuk dapat men-decode file .txt bersama sama menggunakan thread perlu dibuat fungsi thread sebagai berikut.

```c
void *decode(void *arg);
```

Adapun langkah-langkah yang dilakukan dalam fungsi thread `void *decode(void *arg)` adalah pertama dengan mengubah directory ke folder file .txt yang akan diolah dalam hal ini adalah folder `quote`.

```c
     // change directory
    if ((chdir("/home/ariefbadrus/soal-shift-sisop-modul-3-a08-2022/soal1/modul/quote")) < 0) {
        exit(EXIT_FAILURE);
    }
    printf("\n-- change directory to ../modul/quote\n");
```

Kemudian menggunakan `DIR` dan `dirent` untuk dapat mengelola setiap file dalam folder. file `quote.txt` juga disiapkan sebagai penyimpanan output hasil decode nanti.

```c
    DIR *folder;
    struct dirent *ep;

    FILE *quote, *reader;

    folder = opendir(".");

    quote = fopen("/home/ariefbadrus/soal-shift-sisop-modul-3-a08-2022/soal1/modul/quote.txt", "a+");

    if(folder != NULL) {
        while(ep = readdir(folder)) {
            char code[256];
            char in[256];
            char *out;
            size_t out_len;

            if(strstr(ep->d_name, "q")){
                ...
            }
            ...
        }
        ...
    }
```

Selanjutnya mendapatkan code yang tersimpan dalam setiap file .txt menggunakan `fgets()` disimpan kedalam variable `in`.

```c
                // Mendapatkan code
                reader = fopen(ep->d_name, "r");
                if(fgets(code, 256, reader) != NULL) {
                    strcpy(in, code);
                }
```

Kemudian dilakukan decode dengan memanggil fungsi `b64_decode()` yang sudah disiapkan tadi.

```c
                // Decode
                out_len = b64_decodedSize(in) + 1;
                out = malloc(out_len);

                if(!b64_decode(in, (unsigned char *) out, out_len)) {
                    printf("Decode Failure\n");
                    return NULL;
                }
                out[out_len] = '\0';
```

Hasil decode pada variabel `out` disimpan ke dalam text file `quote.txt` menggunakan `fprintf()`. variabel `out` juga dihapus menggunakan `free()` agar bisa diinisialisasi kembali nanti saat perulangan.

```c
                // menyimpan hasil decode ke file quote.txt
                fprintf(quote, "%s\n", out);

                free(out);
```

Langkah yang serupa juga dilakukan untuk folder `music` mulai dari mengubah directory hingga ke menyimpan ke text file.

Untuk source code lengkap mengenai fungsi `void *decode(void *arg)` dapat dilihat dalam file berikut [soal1.c](soal1/soal1.c).

Fungsi thread yang sudah dibuat barusan kemudian dijalankan menggunakan `pthread_create()` dan `pthread_join()`.

```c
    // Decode quote dan music bersama menggunakan base64
    pthread_create(&(tid[2]), NULL, &decode, NULL);
    pthread_join(tid[2], NULL);
```

## c. Memindahkan music.txt dan quote.txt ke folder hasil

Pertama, dilakukan perubahan directory mengarah ke folder modul untuk mempermudah pengerjaan selanjutnya.

```c
    // change directory back to modul
    if ((chdir("/home/ariefbadrus/soal-shift-sisop-modul-3-a08-2022/soal1/modul")) < 0) {
        exit(EXIT_FAILURE);
    }
```

Kemudian membuat process dengan menggunakan `fork()` dan menggunakan `execv()` untuk membuat directory/folder `hasil`

```c
    // Memindahkan music.txt & quote.txt ke folder hasil
    pid_t child1 = fork();
    int status1;
    if (child1 < 0)
        exit(EXIT_FAILURE);

    else if (child1 == 0) {
        // Membuat directory hasil
        char *argv[] = {"mkdir", "hasil", NULL};
        execv("/bin/mkdir", argv);
    }
    else {
        ...
    }
```

Langkah yang serupa juga dilakukan untuk memindahkan file `music.txt` dan `quote.txt` ke folder `hasil`.

```c
    else {
        while((wait(&status1)) > 0);
        printf("\n-- membuat directory hasil berhasil\n");

        pid_t child2 = fork();
        int status2;
        if (child2 < 0)
            exit(EXIT_FAILURE);

        else if (child2 == 0) {
            // Move music.txt dan quote.txt ke folder hasil
            char *argv[] = {"mv", "music.txt", "quote.txt", "/home/ariefbadrus/soal-shift-sisop-modul-3-a08-2022/soal1/modul/hasil", NULL};
            execv("/bin/mv", argv);
        }
        else {
            ..
        }
    }
```

## d. Zip folder hasil dengan password

Untuk melakukan zip folder dapat membuat process dengan menggunakan `fork()` dan `execv()`.

Agar zip folder dapat dilakukan dengan password dapat menambahkan argument `-P` dan password yang akan dimasukkan. Dalam hal ini passwordnya adalah `minihomenestarief`.

```c
        else {
            while((wait(&status2)) > 0);
            printf("\n-- memindahkan music.txt dan quote.txt ke folder hasil\n");

            pid_t child3 = fork();
            int status3;
            if (child3 < 0)
                exit(EXIT_FAILURE);

            else if (child3 == 0) {
                // zip folder hasil
                execl("/bin/zip", "zipHasil", "-r", "-P", "mihinomenestarief", "hasil.zip", "hasil/", NULL);
            }
            else {
                ...
            }
        }
```

Tidak lupa agar menghapus folder hasil lama yang barusan telah dilakukan zip. Hal ini dapat dilakukan juga dengan process.

```c
            else {
                while((wait(&status3)) > 0);
                printf("\n-- zip folder hasil\n");

                pid_t child4 = fork();
                int status4;
                if (child4 < 0)
                    exit(EXIT_FAILURE);

                else if (child4 == 0) {
                    // menghapus folder hasil
                    char * argv[] = {"rm", "-r", "hasil", NULL};
                    execv("/bin/rm", argv);
                }
                else {
                    printf("\n-- menghapus folder hasil\n");
                }
            }
```

## e. Unzip hasil.txt dan membuat file no.txt bersama-sama menggunakan thread

Pertama perlu disiapkan fungsi thread agar dapat menjalankan tugas-tugas tersebut.

```c
void *unzipHasil(void *arg);
void *makeNoTxt(void *arg);
```

Pada fungsi `void *unzipHasil(void *arg)` dibuat process untuk dapat meng-unzip file serta menghapus file zip lama. Menggunakan `fork()`, `execl()`, dan `execv()` sebagai berikut/

```c
void *unzipHasil(void *arg){
    pid_t child = fork();
    int status;
    if (child < 0)
        exit(EXIT_FAILURE);

    else if (child == 0) {
        // unzip hasil.zip
        execl("/bin/unzip", "unzipHasil", "-P", "mihinomenestarief", "/home/ariefbadrus/soal-shift-sisop-modul-3-a08-2022/soal1/modul/hasil.zip", NULL);
    }
    else {
        while((wait(&status)) > 0);
        printf("\n-- unzip hasil berhasil\n");

        pid_t child1 = fork();
        if (child1 < 0)
            exit(EXIT_FAILURE);

        else if (child1 == 0) {
            // menghapus folder hasil
            char * argv[] = {"rm", "-r", "hasil.zip", NULL};
            execv("/bin/rm", argv);
        }
        else {
            printf("\n-- menghapus hasil.zip lama berhasil\n");
        }
    }
}
```

Hal serupa juga dilakukan pada fungsi `void *makeNoTxt(void *arg)` untuk membuat file `no.txt` menggunakan `touch`.

```c
void *makeNoTxt(void *arg){
    pid_t child = fork();
    int status;
    if (child < 0)
        exit(EXIT_FAILURE);

    else if (child == 0) {
        sleep(1);
        execl("/bin/touch", "makeNoTxt", "hasil/no.txt", NULL);
    } else {
        printf("\n-- membuat file no.txt\n");
        wait(0);
    }
}
```

Fungsi thread yang sudah dibuat barusan kemudian dijalankan menggunakan `pthread_create()` dan `pthread_join()`.

```c
    // Unzip hasil.zip menggunakan thread & membuat file no.txt
    pthread_create(&(tid[3]), NULL, &unzipHasil, NULL);
    pthread_create(&(tid[4]), NULL, &makeNoTxt, NULL);
    pthread_join(tid[3], NULL);
    pthread_join(tid[4], NULL);
```

Karena file `no.txt` masih kosong, maka untuk dapat menuliskan kata pada file sesuai permintaan soal dapat menggunakan `fprintf()`.

```c
    FILE * file;
    file = fopen("hasil/no.txt", "w+");[x] a

[x] b

[x] c

[x] d

[x] e

[x] f

[] g
    fprintf(file, "%s\n", "No");
    fclose(file);
```

Kemudian dengan membuat process dengan `fork()` dan `execv()` dilakukan zip folder kembali dimana didalamnya sekarang terdapat 3 file yaitu `music.txt`, `quote.txt`, dan `no.txt`.

```c
    // Zip music.txt, quote.txt, no.txt
    pid_t child5 = fork();
    int status5;
    if (child5 < 0)
        exit(EXIT_FAILURE);

    else if (child5 == 0) {
        execl("/bin/zip", "zipHasil", "-P", "mihinomenestarief", "hasil.zip", "hasil/music.txt", "hasil/quote.txt", "hasil/no.txt", NULL);
    }
    else {
        while((wait(&status5)) > 0)
        printf("\n-- zip folder hasil baru\n");
    }
```

## f. Hasil

- Compile `soal1.c` dengan syntax `gcc -pthread -o soal1 soal1.c && ./soal1`

![1.1](foto/1.1.png)

- Isi folder `modul`

![1.2](foto/1.2.png)

- Isi dalam `hasil.zip` terakhir

![1.3](foto/1.3.png)

- File `music.txt`, `quote.txt`, dan `txt`

![1.4](foto/1.4.png)

## g. Kesulitan

Adapun kesulitan yang kami hadapi selama mengerjakan soal ini adalah sebagai berikut.

- Kurang paham mengenai konsep thread sehingga sering terjadi `segmentation fault` karena mungkin adanya sumber daya/resource yang dipakai bersamaan
- Terjadi error `corrupted size vs. prev_size` saat melakukan perintah `fprintf()` pada file `no.txt`. Sehingga penyelesaiannya adalah melakukan perintah `fprintf()` diluar thread.
- Saat minggu pengerjaan soal shift bertabrakan dengan banyaknya tugas dan praktikum bersamaan.

---

# Soal 2: Platform Online Judge

## a. Register dan Login

server.c

```c
    if(strcmp(reg.msg, "register")==0){
        printf("masuk register\n");
        send_msg("allow");
        ID = rcv_msg();
        UNAME = ID;
        strcat(ID.msg, ":");
        PASS = rcv_msg();
        strcat(ID.msg, PASS.msg);
        ID.charRange=strlen(ID.msg);
        // adding to files
        if( check_req(UNAME.msg, PASS.msg) ){
            w_files(ID.msg);
            send_msg("register");
        } else{
            printf("Client DITOLAK\n");
            send_msg("deny register");
        }
    }
    else if(strcmp(reg.msg, "login")==0){
        send_msg("allow");
        ...
    }else{
        send_msg("deny");
        printf("Client DITOLAK\n");
    }

```

client.c

```c
void get_input(){
    msg reg = rcv_msg();
    printf("%s", reg.msg);
    // confirm
    char charReg[1024];
    gets(charReg);
    send_msg(charReg);
    msg conf = rcv_msg();
    if(strcmp(conf.msg, "allow")==0){
        // send uname
        char UNAME[1024], PASS[1024];
        printf("Masukkan username: ");
        gets(UNAME);
        send_msg(UNAME);
        // send pass
        printf("Masukkan password: ");
        gets(PASS);
        send_msg(PASS);
        msg conf_input=rcv_msg();
        if(strcmp(conf_input.msg, "register")==0){
            printf("Register success\n");
        } else if(strcmp(conf_input.msg, "deny register")==0)
        {
            printf("Register failed\n");
        }

        if(strcmp(conf_input.msg, "login")==0){
            printf("masuk login\n");
            send_msg("allow");
            ID = rcv_msg();
            UNAME = ID;
            strcat(ID.msg, ":");
            PASS = rcv_msg();
            strcat(ID.msg, PASS.msg);
            ID.charRange=strlen(ID.msg);
            if(user_exist(ID.msg)){
        ...
        }
    ...
    }
...
}
```

Jika client berhasil untuk login atau register maka server akan mengirimkan msg `allow`. Sedangkan ketika client ditolak.Server akan mengirimkan pesan `deny`. Pada register, otentifikasi dilakukan pada fungsi `check_req()` dengan return value 0 atau 1 (false dan true).

```c
int check_req(char uname[], char pass[]){
    FILE *fp = fopen("user.txt", "r");
    char buffer[1024];
    int status_uname=1;
    int status_six=1;
    int status_upcase=0;
    int status_locase=0;
    while (fgets(buffer, 1024, fp) != NULL )
    {
        char id[1024], password[1024];
        char *token = strtok(buffer, ":");
        strcpy(id, token);
        token = strtok(NULL, "\n");
        strcpy(password, token);
        // printf("%s %s\n", id, password);
        if(strcmp(uname, id)==0){
            status_uname=0;
            break;
        }
    }
    fclose(fp);
    int len_pass= strlen(pass);
    if(len_pass < 6)status_six=0;

    for(int i=0; i<len_pass; i++ ){
        if(islower(pass[i])) status_locase=1;
        if(isupper(pass[i])) status_upcase=1;
    }
    return (status_uname && status_six && status_locase && status_upcase);
}
```

Pada fungsi `check_req()`, client akan diminta username dan password yang kemudian akan dicek satu persatu dengan file `user.txt` sesuai dengan ketentuan. ketika terdapat indikator yang tidak sesuai, maka akan diset false sebagai tanda bahwa username sudah ada atau password tidak sesuai dengan ketentuan. Apabila username dan password sudah unik akan ditulis pada file user.txt. Sebelum ditulis melalui fungsi `w_files()`. username dan password diubah terlebih dahulu menjadi `username:password` menggunakan `strcat()`.

```c
void w_files(char input[]){
    FILE *fp = fopen("user.txt", "a");
    fprintf(fp, "%s\n", input);
    fclose(fp);
    printf("SUCCESS ADDING TO FILE\n");
}
```

pada saat login, username dan password akan dicek melalui fungsi `user_exist()`.

```c
int user_exist(char user[]){
    printf("user %s\n",user);
    strcat(user, "\n");
    FILE *fp = fopen("user.txt", "r");
    char buffer[1024];
    while (fgets(buffer, 1024, fp) != NULL )
    {
        printf("%s %ld\n",buffer, strlen(buffer));
        if(strcmp(buffer, user)==0) return 1;
    }
    fclose(fp);
    return 0;
}
```

fungsi `user_exist()` akan mengecek username dan password dengan file `user.txt`.

## b. Sistem memiliki sebuah database pada server untuk menampung problem atau soal-soal yang ada pada online judge. Database ini bernama problems.tsv yang terdiri dari judul problem dan author problem (berupa username dari author), yang dipisah dengan \t. File otomatis dibuat saat server dijalankan.

Pada fungsi main terdapat 2 thread yang berfungsi untuk membuat file `problem.tsv`

```c
	while(i<2) // loop sejumlah thread
	{
		err=pthread_create(&(tid[i]),NULL,&playandcount,NULL); //membuat thread
		if(err!=0) //cek error
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
		i++;
	}
	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
	exit(0);
```

```c
void* playandcount(void *arg)
{
	char *argv1[] = {"clear", NULL};
	char *argv2[] = {"xlogo", NULL};
	unsigned long i=0;
	pthread_t id=pthread_self();
	int iter;
	if(pthread_equal(id,tid[0])) //thread untuk clear layar
	{
        get_input();
	}
	else if(pthread_equal(id,tid[1])) // thread menampilkan counter
	{
            FILE *fp;
            fp = fopen("problems.tsv", "a+");
            if(!fp){
                printf("problems.tsv FAILED CREATED ");
            }
            fclose(fp);
	}
	return NULL;
}

```

pada fungsi `playandcount()` akan melakukan dua hal sekaligus, yaitu get_input dan membuat file `problem.tsv`.

## c. Client yang telah login, dapat memasukkan command yaitu ‘add’ yang berfungsi untuk menambahkan problem/soal baru pada sistem. Saat client menginputkan command tersebut, server akan meminta beberapa input

ketika client mengirimkan pesan `add` maka server akan melakukan menerima tiga pesan sekaligus, yaitu description, input, dan output.

```c
    if(strcmp(select.msg, "add")==0){
        // printf("masuk add\n");
        msg title, path_desc, path_input, path_output;
        title = rcv_msg();
        path_desc = rcv_msg();
        path_input = rcv_msg();
        path_output = rcv_msg();
        add_problem(UNAME.msg, title.msg);
        add_file(title.msg, path_desc.msg, path_input.msg, path_output.msg);
        input_file(title.msg, path_desc.msg, path_input.msg, path_output.msg);
    }

```

pada fungsi `add_problem()`, server akan menambahkan judul problem dan author ke dalam file `problem.tsv`.

```c
void add_problem(char uname[1024], char title[1024]){
    FILE *fp;
    //open, if not exist make file
    fp = fopen("problems.tsv", "a+");
    if (fp == NULL)
    {
        printf("File cannot be opened");
    }
    fprintf(fp, "%s\t%s\n", title, uname);
    fclose(fp);
}
```

Fungsi `add_file()` akan membuat direktori pada server sesuai dengan judul yang telah dituliskan.

```c
void add_file(char title[1024], char path_desc[1024],
char path_input[1024], char path_output[1024]){
    int make_dir = mkdir(title, 0777);
}
```

Fungsi `input_file()` akan membuat file `description.txt`, `input.txt`, dan `output.txt` kemudian menuliskan isinya sesuai dengan pesan dari client.

```c
void input_file(char title[1024], char path_desc[1024],
char path_input[1024], char path_output[1024]){

    char dpath[1024], ipath[1024], opath[1024];
    sprintf(dpath, "%s/description.txt", title);
    sprintf(ipath, "%s/input.txt", title);
    sprintf(opath, "%s/output.txt", title);
    // desc
    FILE *desc = fopen(dpath, "a");
    fprintf(desc, "%s\n", path_desc);
    fclose(desc);

    // input
    FILE *input = fopen(ipath, "a");
    fprintf(input, "%s\n", path_input);
    fclose(input);

    // output
    FILE *output = fopen(opath, "a");
    fprintf(output, "%s\n", path_output);
    fclose(output);
}
```

## d. Client yang telah login, dapat memasukkan command ‘see’ yang berguna untuk menampilkan seluruh judul problem yang ada beserta authornya(author merupakan username client yang menambahkan problem tersebut). Format yang akan ditampilkan oleh server adalah sebagai berikut: `judul-problem-1 by author1`

server.c

```c
    else if(strcmp(select.msg, "see")==0){
        show_data();
    }
```

```c
void show_data(){
    FILE *fp = fopen("problems.tsv", "r");
    if(!fp){
        printf("FILE CANNOT OPEN\n");
        // exit();
    }
    char buffer[1024];
    printf("========DATA AUTHOR=========\n");
    while (fgets(buffer, 1024, fp) != NULL )
    {
        send_msg("1");
        char title[1024], author[1024];
        char *token = strtok(buffer, "\t");
        strcpy(title, token);
        token = strtok(NULL, "\n");
        strcpy(author, token);
        char tempo[1024]={0};
        strcat(tempo, title);
        strcat(tempo, " by ");
        strcat(tempo, author);
        send_msg(tempo);
        // send_msg
        printf("%s by %s\n", title, author);
    }
    fclose(fp);
    send_msg("0");
}
```

client.c

```c
else if(strcmp(select, "see")==0){
    printf("========DATA AUTHOR=========\n");
    msg rcv_file = rcv_msg();
    while(strcmp(rcv_file.msg, "0") != 0){
        rcv_file = rcv_msg();
        printf("%s\n", rcv_file.msg);
        rcv_file = rcv_msg();
    }
}
```

ketika server menerima pesan see, maka program akan memanggil fungsi show data dan mengirimkan judul dan author kepada client untuk ditampilkan. Sebelum judul dan author dikirim, string dikelola terlebih dahulu dengan `strcat()`.

## e. Client yang telah login, dapat memasukkan command ‘download judul-problem’ yang berguna untuk mendownload file description.txt dan input.txt yang berada pada folder pada server dengan nama yang sesuai dengan argumen kedua pada command yaitu judul-problem. Kedua file tersebut akan disimpan ke folder dengan nama judul-problem di client.

server.c

```c
else if(strcmp(select.msg, "download")==0){
    msg title_file = rcv_msg();
    socket_send_file(title_file.msg);
    // printf("masuk download\n");
}
```

```c
void socket_send_file(char input[]){
    printf("judul %s\n",input);
    char desc_path[1024]={0}, output_path[1024]={0};
    strcat(desc_path, input);
    strcat(desc_path, "/description.txt");
    strcat(output_path, input);
    strcat(output_path, "/output.txt");
    printf("path send file %s %s\n", desc_path, output_path );
    FILE *fp_desc = fopen(desc_path, "r");
    if(!fp_desc){
        printf("DESC UNSUCCESS\n");
    }
    char buffer[1024];
    while (fgets(buffer, 1024, fp_desc) != NULL )
    {
        send_msg("1");
        send_msg(buffer);
        printf("%s %ld\n",buffer, strlen(buffer));
    }
    fclose(fp_desc);

    FILE *fp_inp = fopen(output_path, "r");
    if(!fp_inp){
        printf("INPUT UNSUCCESS\n");
    }
    char temp[1024];
    while (fgets(temp, 1024, fp_desc) != NULL )
    {
        send_msg("2");
        send_msg(temp);
        printf("%s %ld\n",temp, strlen(temp));
    }
    send_msg("0");
    fclose(fp_inp);
}
```

client.c

```c
else if(strcmp(select, "download")==0){
    // printf("masuk download\n");
    printf("Masukkan judul file: ");
    char title_file[1024];
    gets(title_file);
    send_msg(title_file);
    add_file(title_file);

    char desc_path[1024]={0}, output_path[1024]={0};
    strcat(desc_path, title_file);
    strcat(desc_path, "/description.txt");
    strcat(output_path, title_file);
    strcat(output_path, "/output.txt");
    // open the file
    FILE *fp_desc = fopen(desc_path, "a");
    FILE *fp_inp = fopen(output_path, "a");
    msg comp = rcv_msg();
    // fprintf(fp_desc, "%s", "cpba");
    // fprintf(fp_inp, "%s", "onpu");
    while (strcmp(comp.msg, "0") != 0 )
    {
        if(strcmp(comp.msg, "1")==0){
            msg desc_msg = rcv_msg();
            fprintf(fp_desc, "%s",desc_msg.msg);
            printf("1 == %s\n", desc_msg.msg);
        }

        if(strcmp(comp.msg, "2")==0){
            msg input_msg = rcv_msg();
            fprintf(fp_inp, "%s", input_msg.msg);
            printf("2 == %s\n", input_msg.msg);
        }
        comp = rcv_msg();
    }

    fclose(fp_desc);
    fclose(fp_inp);
    printf("DOWNLOAD FINISH\n");
}
```

pada saat client mengetik `download`. client akan mengirimkan pesan `download`. Kemudian server akan mengirimkan isi file satu persatu mulai dari description.txt lalu output.txt. Pada client, akan membuat folder sesuai dengan judul kemudian membuat dan menerima file description.txt dan output.txt. Ketika pesan dari server adalah 0 menandakan proses sudah berakhir. ketika pesan 1 menandakan `description.txt`, ketika pesan 2 menandakan `output.txt`.

## f. Client yang telah login, dapat memasukkan command ‘submit judul-problem path-file-output.txt’. Command ini berguna untuk melakukan submit jawaban dari client terhadap problem tertentu. Algoritma yang dijalankan adalah client akan mengirimkan file output.txt nya melalui argumen ke 3 pada command, lalu server akan menerima dan membandingkan isi file output.txt yang telah dikirimkan oleh client dan output.txt yang ada pada folder dengan nama yang sesuai dengan argumen ke 2 pada command. Jika file yang dibandingkan sama, maka server akan mengirimkan pesan “AC” dan jika tidak maka server akan mengeluarkan pesan “WA”.

```c
    else if(strcmp(select.msg, "submit")==0){
        // printf("masuk submit\n");
        msg title_submit=rcv_msg();
        msg path_submit=rcv_msg();
        printf("path submit %s\n", path_submit.msg);
        printf("title submit %s\n", title_submit.msg);
        FILE *fp_path = fopen(path_submit.msg, "r");
        if(!fp_path){
            printf("PATH FILE CANNOT OPEN\n");
        }
        // // di server
        char path_server[1024]={0};
        strcat(path_server, title_submit.msg);
        strcat(path_server, "/output.txt");
        printf("DI SERVER %s\n", path_server);
        FILE *fp_title = fopen(path_server, "r");
        if(!fp_title){
            printf("TITLE FILE CANNOT BE OPEN\n");
        }
        char buffer[1024]={0};
        char bufferbuffer[1024]={0};
        int status = 1;
        while (fgets(buffer, 1024, fp_path) != NULL )
        {
            printf("=>> %s\n", buffer);
            if(fgets(bufferbuffer, 1024, fp_title)!=NULL){
                if(strcmp(buffer, bufferbuffer) != 0){
                    status = 0;
                    break;
                }
            } else{
                status = 0;
                break;
            }
        }
        fclose(fp_path);
        fclose(fp_title);
        if(status==1)send_msg("AC");
        else send_msg("WA");
    }
```

ketika client memberikan pesan submit, server akan menerima output.txt dari client kemudian mengecek dengan output pada server. kemudian tiap baris akan dicek. Apabila tidak sama maka status akan berubah menjadi 0 dan server akan mengirimkan pesan `WA` pada client Sebaliknya apabila status 1 maka server mengirimkan `AC`.

## Hasil

- Pilihan ketika pertama kali client masuk

![2.1](foto/2.1.png)

- ketika register sukses

![2.2](foto/2.2.png)

- isi dari file user.txt

![2.3](foto/2.3.png)

- isi dari perintah add / menambahkan folder dan file sesuai dengan judul, deskripsi, input, dan output.

![2.4](foto/2.4.png)

- isi file `ini_problem` yang baru saja ditambahkan.

![2.5](foto/2.5.png)

- tampilan ketika client memberikan perintah `see`

![2.6](foto/2.6.png)

- perintah `see` merupakan isi dari file `problem.tsv`

![2.7](foto/2.7.png)

- program ketika client memberikan perintah `download`

![2.8](foto/2.8.png)

- isi dari `description.txt` dan `output.txt` yang telah didownload client

![2.9](foto/2.9.png)

- program ketika client memberikan perintah submit. Pada kasus ini client mendapatkan `WA` karena output client tidak sama dengan server.

![2.10](foto/2.10.png)

- isi dari output.txt client

![2.11](foto/2.11.png)

- program ketika client memberikan perintah submit. Pada kasus ini client mendapatkan `AC` karena output client sama dengan server.

![2.12](foto/2.12.png)

- - isi dari output.txt server dan client sama

![2.13](foto/2.13.png)

## kesulitan

- Waktu pengerjaan praktikum sisop P3 bersamaan dengan praktikum probstat dan tugas maupun ujian ETS sehingga waktu yang dialokasikan untuk praktikum sisop sangatlah sedikit.

# Soal 3

## kesulitan

- Waktu pengerjaan praktikum sisop P3 bersamaan dengan praktikum probstat dan tugas maupun ujian ETS sehingga waktu yang dialokasikan untuk praktikum sisop sangatlah sedikit.

## Saran

- praktikum bisa dimulai pada hari jumat karena akan ada waktu luang pada hari jumat, sabtu, dan minggu dan tidak bersamaan dengan praktikum probstat supaya alokasi waktu untuk praktikum lebih banyak.
