#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include<pthread.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>

#define PORT 3001


#define current_path "/home/ram/sisop/M3/Praktikum/code/soal2/Server"

int socketID;

// SOP RECEIVE
// int ambil;
// char setering[1024]={0};
// ambil = read( socketID , setering, 1024);
// printf("%d %s\n", ambil, setering );

typedef struct message
{
    int charRange;
    char msg[1024];
}msg;

void send_msg(char input[]){
    send(socketID, input, 1024, 0);
}

void socket_send_file(char input[]){
    printf("judul %s\n",input);
    char desc_path[1024]={0}, output_path[1024]={0};
    strcat(desc_path, input);
    strcat(desc_path, "/description.txt");
    strcat(output_path, input);
    strcat(output_path, "/output.txt");
    printf("path send file %s %s\n", desc_path, output_path );
    FILE *fp_desc = fopen(desc_path, "r");
    if(!fp_desc){
        printf("DESC UNSUCCESS\n");
    }
    char buffer[1024];
    while (fgets(buffer, 1024, fp_desc) != NULL )
    {
        send_msg("1");
        send_msg(buffer);
        printf("%s %ld\n",buffer, strlen(buffer));
    }
    fclose(fp_desc);

    FILE *fp_inp = fopen(output_path, "r");
    if(!fp_inp){
        printf("INPUT UNSUCCESS\n");
    }
    char temp[1024];
    while (fgets(temp, 1024, fp_desc) != NULL )
    {
        send_msg("2");
        send_msg(temp);
        printf("%s %ld\n",temp, strlen(temp));
    }
    send_msg("0");
    fclose(fp_inp);
}

msg rcv_msg(){
    msg get_msg;
    // char buffer[1024]={0};
    get_msg.charRange = read( socketID , get_msg.msg, 1024);
    // printf("%d %s %ld\n", ambil, setering, strlen(setering) );
    return get_msg;
}

int user_exist(char user[]){
    printf("user %s\n",user);
    strcat(user, "\n");
    FILE *fp = fopen("user.txt", "r");
    char buffer[1024];
    while (fgets(buffer, 1024, fp) != NULL )
    {
        printf("%s %ld\n",buffer, strlen(buffer));
        if(strcmp(buffer, user)==0) return 1;
    }
    fclose(fp);
    return 0;
}

void w_files(char input[]){
    FILE *fp = fopen("user.txt", "a"); 
    fprintf(fp, "%s\n", input);
    fclose(fp);
    printf("SUCCESS ADDING TO FILE\n");
}

int check_req(char uname[], char pass[]){
    FILE *fp = fopen("user.txt", "r");
    char buffer[1024];
    int status_uname=1;
    int status_six=1;
    int status_upcase=0;
    int status_locase=0;
    while (fgets(buffer, 1024, fp) != NULL )
    {
        char id[1024], password[1024];
        char *token = strtok(buffer, ":");
        strcpy(id, token);
        token = strtok(NULL, "\n");
        strcpy(password, token);
        // printf("%s %s\n", id, password);
        if(strcmp(uname, id)==0){
            status_uname=0;
            break;
        }
    }
    fclose(fp);
    int len_pass= strlen(pass);
    if(len_pass < 6)status_six=0;
    
    for(int i=0; i<len_pass; i++ ){
        if(islower(pass[i])) status_locase=1;
        if(isupper(pass[i])) status_upcase=1;
    }
    return (status_uname && status_six && status_locase && status_upcase);
}

void add_problem(char uname[1024], char title[1024]){
    FILE *fp;
    //open, if not exist make file
    fp = fopen("problems.tsv", "a+");
    if (fp == NULL)
    {
        printf("File cannot be opened");
    }
    fprintf(fp, "%s\t%s\n", title, uname);
    fclose(fp);
}

void add_file(char title[1024], char path_desc[1024], 
char path_input[1024], char path_output[1024]){
    int make_dir = mkdir(title, 0777);
}

void input_file(char title[1024], char path_desc[1024], 
char path_input[1024], char path_output[1024]){
    
    char dpath[1024], ipath[1024], opath[1024];
    sprintf(dpath, "%s/description.txt", title);
    sprintf(ipath, "%s/input.txt", title);
    sprintf(opath, "%s/output.txt", title);
    // desc
    FILE *desc = fopen(dpath, "a"); 
    fprintf(desc, "%s\n", path_desc);
    fclose(desc);

    // input
    FILE *input = fopen(ipath, "a"); 
    fprintf(input, "%s\n", path_input);
    fclose(input);

    // output
    FILE *output = fopen(opath, "a"); 
    fprintf(output, "%s\n", path_output);
    fclose(output);
}

void show_data(){
    FILE *fp = fopen("problems.tsv", "r");
    if(!fp){
        printf("FILE CANNOT OPEN\n");
        // exit();
    }
    char buffer[1024];
    printf("========DATA AUTHOR=========\n");
    while (fgets(buffer, 1024, fp) != NULL )
    {
        send_msg("1");
        char title[1024], author[1024];
        char *token = strtok(buffer, "\t");
        strcpy(title, token);
        token = strtok(NULL, "\n");
        strcpy(author, token);
        char tempo[1024]={0};
        strcat(tempo, title);
        strcat(tempo, " by ");
        strcat(tempo, author);
        send_msg(tempo);
        // send_msg
        printf("%s by %s\n", title, author);
    }
    fclose(fp);
    send_msg("0");
}


void get_input(){
    // kirim pilihan
    send_msg("Pilih Salah Satu\n1. register\n" "2. login\n" "Pilihan(huruf): ");
    msg reg = rcv_msg();
    printf("%d \n%s\n %ld\n",reg.charRange, reg.msg, strlen(reg.msg));
    msg ID, UNAME, PASS;
    if(strcmp(reg.msg, "register")==0){
        printf("masuk register\n");
        send_msg("allow");
        ID = rcv_msg();
        UNAME = ID;
        strcat(ID.msg, ":");
        PASS = rcv_msg();
        strcat(ID.msg, PASS.msg);
        ID.charRange=strlen(ID.msg);
        // adding to files
        if( check_req(UNAME.msg, PASS.msg) ){
            w_files(ID.msg);
            send_msg("register");
            // printf("YEE MAZUKK\n");
        } else{
            printf("Client DITOLAK\n");
            send_msg("deny register");    
        }
    }
    else if(strcmp(reg.msg, "login")==0){
        printf("masuk login\n");
        send_msg("allow");
        ID = rcv_msg();
        UNAME = ID;
        strcat(ID.msg, ":");
        PASS = rcv_msg();
        strcat(ID.msg, PASS.msg);
        ID.charRange=strlen(ID.msg);
        if(user_exist(ID.msg)){
            send_msg("login");
            msg select = rcv_msg();
            if(strcmp(select.msg, "add")==0){
                // printf("masuk add\n");
                msg title, path_desc, path_input, path_output;
                title = rcv_msg();
                path_desc = rcv_msg();
                path_input = rcv_msg();
                path_output = rcv_msg();
                add_problem(UNAME.msg, title.msg);
                add_file(title.msg, path_desc.msg, path_input.msg, path_output.msg);
                input_file(title.msg, path_desc.msg, path_input.msg, path_output.msg);
            }
            else if(strcmp(select.msg, "see")==0){
                show_data();
                // printf("masuk see\n");
            }
            else if(strcmp(select.msg, "download")==0){
                msg title_file = rcv_msg();
                socket_send_file(title_file.msg);
                // printf("masuk download\n");
            }
            else if(strcmp(select.msg, "submit")==0){
                // printf("masuk submit\n");
                msg title_submit=rcv_msg();
                msg path_submit=rcv_msg();
                printf("path submit %s\n", path_submit.msg);
                printf("title submit %s\n", title_submit.msg);
                FILE *fp_path = fopen(path_submit.msg, "r");
                if(!fp_path){
                    printf("PATH FILE CANNOT OPEN\n");
                }
                // // di server
                char path_server[1024]={0};
                strcat(path_server, title_submit.msg);
                strcat(path_server, "/output.txt");
                printf("DI SERVER %s\n", path_server);
                FILE *fp_title = fopen(path_server, "r");
                if(!fp_title){
                    printf("TITLE FILE CANNOT BE OPEN\n");
                }
                char buffer[1024]={0};
                char bufferbuffer[1024]={0};
                int status = 1; 
                while (fgets(buffer, 1024, fp_path) != NULL )
                {
                    printf("=>> %s\n", buffer);
                    if(fgets(bufferbuffer, 1024, fp_title)!=NULL){
                        if(strcmp(buffer, bufferbuffer) != 0){
                            status = 0;
                            break;
                        }
                    } else{
                        status = 0;
                        break;
                    }
                }
                fclose(fp_path);
                fclose(fp_title);
                if(status==1)send_msg("AC");
                else send_msg("WA");
            }
        }
        else{
            printf("Client DITOLAK\n");
        }
    }else{
        send_msg("deny");
        printf("Client DITOLAK\n");        
    }
}

pthread_t tid[2]; //inisialisasi array untuk menampung thread dalam kasus ini ada 2 thread
pid_t child;

int length=5; //inisialisasi jumlah untuk looping
void* playandcount(void *arg)
{
	char *argv1[] = {"clear", NULL};
	char *argv2[] = {"xlogo", NULL};
	unsigned long i=0;
	pthread_t id=pthread_self();
	int iter;
	if(pthread_equal(id,tid[0])) //thread untuk clear layar
	{
        get_input();
	}
	else if(pthread_equal(id,tid[1])) // thread menampilkan counter
	{
            FILE *fp;
            fp = fopen("problems.tsv", "a+");
            if(!fp){
                printf("problems.tsv FAILED CREATED ");
            }
            fclose(fp);
	}
	// else if(pthread_equal(id,tid[2])) // thread menampilkan gambar
	// {
    //     child = fork();
    //     if (child==0) {
	// 	    execv("/usr/bin/xlogo", argv2);
	//     }
	// }

	return NULL;
}

int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *hello = "Hello from server";
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    } else{
        socketID = new_socket;
    }

    // SOP RECEIVE
    // int ambil;
    // char setering[1024]={0};
    // ambil = read( socketID , setering, 1024);
    // printf("%d %s\n", ambil, setering );

    int i=0;
	int err;
	while(i<2) // loop sejumlah thread
	{
		err=pthread_create(&(tid[i]),NULL,&playandcount,NULL); //membuat thread
		if(err!=0) //cek error
		{
			printf("\n can't create thread : [%s]",strerror(err));
		}
		else
		{
			printf("\n create thread success\n");
		}
		i++;
	}
	pthread_join(tid[0],NULL);
	pthread_join(tid[1],NULL);
	exit(0);

    return 0;
}