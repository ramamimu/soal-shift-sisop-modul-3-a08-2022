#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#define PORT 3001
  
int socketID;

// SOP SEND
// char *ini = "lagi dari client";
// send(sock, ini, strlen(ini), 0);

typedef struct message
{
    int charRange;
    char msg[1024];
}msg;

void send_msg(char input[]){
    send(socketID, input, 1024, 0);
    
}

msg rcv_msg(){
    msg get_msg;
    char buffer[1024]={0};
    get_msg.charRange = read( socketID , get_msg.msg, 1024);
    // printf("%d %s %ld\n", ambil, setering, strlen(setering) );
    return get_msg;
}

void add_file(char title[1024]){
    int make_dir = mkdir(title, 0777);
}

void get_input(){
    msg reg = rcv_msg();
    printf("%s", reg.msg);
    // printf("%d \n%s\n %ld\n",reg.charRange, reg.msg, strlen(reg.msg));
    // confirm
    char charReg[1024];
    gets(charReg);
    send_msg(charReg);
    msg conf = rcv_msg();
    if(strcmp(conf.msg, "allow")==0){
        // send uname
        char UNAME[1024], PASS[1024];
        printf("Masukkan username: ");
        gets(UNAME);
        send_msg(UNAME);
        // send pass
        printf("Masukkan password: ");
        gets(PASS);
        send_msg(PASS);
        msg conf_input=rcv_msg();
        if(strcmp(conf_input.msg, "register")==0){
            printf("Register success\n");
        } else if(strcmp(conf_input.msg, "deny register")==0)
        {
            printf("Register failed\n");
        }
        
        if(strcmp(conf_input.msg, "login")==0){
            printf("Login success\n");            
            printf("\n========================\n");            
            printf("Masukkan perintah:\n1. add\n2. see\n3. download\n4. submit\nPerintah(huruf): ");
            char select[1024];
            gets(select);
            // printf("selec.t %s\n",select);
            send_msg(select);
            if(strcmp(select, "add")==0){
                // printf("masuk add\n");
                char title[1024];
                printf("Judul problem: ");
                gets(title);
                send_msg(title);

                char path_desc[1024];
                printf("Filepath description.txt: ");
                gets(path_desc);
                send_msg(path_desc);

                char path_input[1024];
                printf("Filepath input.txt: ");
                gets(path_input);
                send_msg(path_input);

                char path_output[1024];
                printf("Filepath output.txt: ");
                gets(path_output);
                send_msg(path_output);
            }
            else if(strcmp(select, "see")==0){
                // printf("masuk see\n");
                // sprintf
                printf("========DATA AUTHOR=========\n");
                msg rcv_file = rcv_msg();
                while(strcmp(rcv_file.msg, "0") != 0){
                    rcv_file = rcv_msg();
                    printf("%s\n", rcv_file.msg);
                    rcv_file = rcv_msg();
                }
            }
            else if(strcmp(select, "download")==0){
                // printf("masuk download\n");
                printf("Masukkan judul file: ");
                char title_file[1024];
                gets(title_file);
                send_msg(title_file);
                add_file(title_file);

                char desc_path[1024]={0}, output_path[1024]={0};
                strcat(desc_path, title_file);
                strcat(desc_path, "/description.txt");
                strcat(output_path, title_file);
                strcat(output_path, "/output.txt");
                // open the file
                FILE *fp_desc = fopen(desc_path, "a");
                FILE *fp_inp = fopen(output_path, "a");
                msg comp = rcv_msg();
                // fprintf(fp_desc, "%s", "cpba");
                // fprintf(fp_inp, "%s", "onpu");
                while (strcmp(comp.msg, "0") != 0 )
                {
                    if(strcmp(comp.msg, "1")==0){
                        msg desc_msg = rcv_msg();
                        fprintf(fp_desc, "%s",desc_msg.msg);
                        printf("1 == %s\n", desc_msg.msg);
                    }

                    if(strcmp(comp.msg, "2")==0){
                        msg input_msg = rcv_msg();
                        fprintf(fp_inp, "%s", input_msg.msg);
                        printf("2 == %s\n", input_msg.msg);
                    }
                    comp = rcv_msg();
                }

                fclose(fp_desc);
                fclose(fp_inp);
                printf("DOWNLOAD FINISH\n");
            }
            else if(strcmp(select, "submit")==0){
                // printf("masuk submit\n");
                char title_submit[1024]={0};
                printf("Masukkan judul: ");
                gets(title_submit);
                send_msg(title_submit);
                // path
                // /home/ram/sisop/M3/Praktikum/code/soal2/Client/output/output.txt
                char path_submit[1024]={0};
                printf("Masukkan path: ");
                gets(path_submit);
                send_msg(path_submit);
                printf("%s %s\n",title_submit, path_submit);
                msg get_ans = rcv_msg();
                printf("jawaban anda %s\n",get_ans.msg);
            }
        }
        else if(strcmp(conf_input.msg, "deny login")==0){
            printf("Login failed\n");
        }
    } else{
        printf("Access denied\n");
    }
    // printf("%s:%s\n", UNAME,PASS);
    // printf("%s\n", UNAME);
}

int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int sock = 0;
    struct sockaddr_in serv_addr;
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    } else{
        socketID = sock;
    }
  
    memset(&serv_addr, '0', sizeof(serv_addr));
  
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);
      
    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }
  
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }

    get_input();

    return 0;
}