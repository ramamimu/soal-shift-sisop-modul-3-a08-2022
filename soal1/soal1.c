#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdint.h>
#include <dirent.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/wait.h>

// BASE64 DECODE
// source : https://nachtimwald.com/2017/11/18/base64-encode-and-decode-in-c/
int b64_decodeTable[] = { 62, -1, -1, -1, 63, 52, 53, 54, 55, 56, 57, 58,
	59, 60, 61, -1, -1, -1, -1, -1, -1, -1, 0, 1, 2, 3, 4, 5,
	6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
	21, 22, 23, 24, 25, -1, -1, -1, -1, -1, -1, 26, 27, 28,
	29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42,
	43, 44, 45, 46, 47, 48, 49, 50, 51 };

size_t b64_decodedSize(const char *inputString)
{
	size_t inputLength;
	size_t outputLength;

	size_t i;

	if (inputString == NULL)
		return 0;

	inputLength = strlen(inputString);
	outputLength = inputLength / 4 * 3;

    i = inputLength;
	while(i > 0) {
		if (inputString[i] == '=') {
			outputLength--;
		} else {
			break;
		}
        i--;
	}

	return outputLength;
}

int b64_isValidChar(char c) {
    if (c >= '0' && c <= '9') return 1;
	if (c >= 'A' && c <= 'Z') return 1;
	if (c >= 'a' && c <= 'z') return 1;
	if (c == '+' || c == '/' || c == '=') return 1;

	return 0;
}

int b64_decode(const char *inputString, unsigned char *decodedString, size_t outLength) {
    size_t inputLength;
    int decodeHandler;

    size_t i, j;

    if(inputString == NULL || decodedString == NULL) return 0;

    inputLength = strlen(inputString);
    if(outLength < b64_decodedSize(inputString) || inputLength % 4 != 0) return 0;

    for(i = 0; i<inputLength; i++) {
        if(!b64_isValidChar(inputString[i])) return 0;
    }

    for(i = 0, j = 0; i < inputLength; i+=4, j+=3) {
        decodeHandler = b64_decodeTable[inputString[i]-43];
        decodeHandler = (decodeHandler << 6) | b64_decodeTable[inputString[i+1]-43];

        if(inputString[i+2] == '=') decodeHandler = decodeHandler << 6;
        else decodeHandler = (decodeHandler << 6) | b64_decodeTable[inputString[i+2]-43];

        if(inputString[i+3] == '=') decodeHandler = decodeHandler << 6;
        else decodeHandler = (decodeHandler << 6) | b64_decodeTable[inputString[i+3]-43];

        decodedString[j] = (decodeHandler >> 16) & 0xff;

        if(inputString[i+2] != '=') decodedString[j+1] = (decodeHandler >> 8) & 0xff;
        if(inputString[i+3] != '=') decodedString[j+2] = decodeHandler & 0xff;
    }

    return 1;
}

// Function
void *unzipQuote(void *arg);
void *unzipMusic(void *arg);
void *decode(void *arg);
void *unzipHasil(void *arg);
void *makeNoTxt(void *arg);

// Array untuk menampung thread
pthread_t tid[15];

int main(){
    int err;

    // Membuat directory modul
    pid_t child = fork();
    if (child < 0)
        exit(EXIT_FAILURE);

    else if (child == 0) {
        char *argv[] = {"mkdir", "modul", NULL};
        execv("/bin/mkdir", argv);
    }
    else {
        printf("\n-- membuat directory modul berhasil\n");
        wait(0);
    }

    // Unzip music.zip & quote.zip menggunakan thread
    pthread_create(&(tid[0]), NULL, &unzipQuote, NULL);
    pthread_create(&(tid[1]), NULL, &unzipMusic, NULL);
    pthread_join(tid[0], NULL);
    pthread_join(tid[1], NULL);

    // Decode quote dan music bersama menggunakan base64
    pthread_create(&(tid[2]), NULL, &decode, NULL);
    pthread_join(tid[2], NULL);

    // change directory back to modul
    if ((chdir("/home/ariefbadrus/soal-shift-sisop-modul-3-a08-2022/soal1/modul")) < 0) {
        exit(EXIT_FAILURE);
    }

    // Memindahkan music.txt & quote.txt ke folder hasil
    pid_t child1 = fork();
    int status1;
    if (child1 < 0)
        exit(EXIT_FAILURE);

    else if (child1 == 0) {
        // Membuat directory hasil
        char *argv[] = {"mkdir", "hasil", NULL};
        execv("/bin/mkdir", argv);
    }
    else {
        while((wait(&status1)) > 0);
        printf("\n-- membuat directory hasil berhasil\n");

        pid_t child2 = fork();
        int status2;
        if (child2 < 0)
            exit(EXIT_FAILURE);

        else if (child2 == 0) {
            // Move music.txt dan quote.txt ke folder hasil
            char *argv[] = {"mv", "music.txt", "quote.txt", "/home/ariefbadrus/soal-shift-sisop-modul-3-a08-2022/soal1/modul/hasil", NULL};
            execv("/bin/mv", argv);
        }
        else {
            while((wait(&status2)) > 0);
            printf("\n-- memindahkan music.txt dan quote.txt ke folder hasil\n");

            pid_t child3 = fork();
            int status3;
            if (child3 < 0)
                exit(EXIT_FAILURE);

            else if (child3 == 0) {
                // zip folder hasil
                execl("/bin/zip", "zipHasil", "-r", "-P", "mihinomenestarief", "hasil.zip", "hasil/", NULL);   
            }
            else {
                while((wait(&status3)) > 0);
                printf("\n-- zip folder hasil\n");

                pid_t child4 = fork();
                int status4;
                if (child4 < 0)
                    exit(EXIT_FAILURE);

                else if (child4 == 0) {  
                    // menghapus folder hasil
                    char * argv[] = {"rm", "-r", "hasil", NULL};
                    execv("/bin/rm", argv); 
                }
                else {
                    printf("\n-- menghapus folder hasil\n");
                }
            }
        }
    }


    // Unzip hasil.zip menggunakan thread & membuat file no.txt
    pthread_create(&(tid[3]), NULL, &unzipHasil, NULL);
    pthread_create(&(tid[4]), NULL, &makeNoTxt, NULL);
    pthread_join(tid[3], NULL);
    pthread_join(tid[4], NULL);
    
    FILE * file;
    file = fopen("hasil/no.txt", "w+");
    fprintf(file, "%s\n", "No");
    fclose(file);

    // Zip music.txt, quote.txt, no.txt
    pid_t child5 = fork();
    int status5;
    if (child5 < 0)
        exit(EXIT_FAILURE);

    else if (child5 == 0) {
        execl("/bin/zip", "zipHasil", "-P", "mihinomenestarief", "hasil.zip", "hasil/music.txt", "hasil/quote.txt", "hasil/no.txt", NULL);   
    }
    else {
        while((wait(&status5)) > 0)
        printf("\n-- zip folder hasil baru\n");
    }

    // Menghapus folder music, quote, dan hasil jika diperlukan
    // pid_t child6 = fork();
    // int status6;
    // if (child6 < 0)
    //     exit(EXIT_FAILURE);

    // else if (child6 == 0) {
    //     char *argv[] = {"rm", "-r", "hasil", "music", "quote", NULL};
    //     execv("/bin/rm", argv);
    // }
    // else {
    //     while((wait(&status6)) > 0)
    //     printf("\n-- menghapus folder berhasil\n");
    //     printf("\n======= SELESAI ========\n");
    // }

    exit(0);
    return 0;

}

void *unzipQuote(void *arg){
    pid_t child = fork();
    if (child < 0)
        exit(EXIT_FAILURE);

    else if (child == 0) {
        // unzip quote.zip
        // file quote.zip disiapkan
        execl("/bin/unzip", "unzipQuote", "/home/ariefbadrus/Downloads/quote.zip", "-d", "/home/ariefbadrus/soal-shift-sisop-modul-3-a08-2022/soal1/modul/quote", NULL);
    }
    else 
        wait(0);
    printf("\n-- unzip quote berhasil\n");
}

void *unzipMusic(void *arg){
    pid_t child = fork();
    if (child< 0)
        exit(EXIT_FAILURE);

    else if (child == 0) {
        // unzip music.zip
        // file music.zip disiapkan
        execl("/bin/unzip", "unzipMusic", "/home/ariefbadrus/Downloads/music.zip", "-d", "/home/ariefbadrus/soal-shift-sisop-modul-3-a08-2022/soal1/modul/music", NULL);
    }
    else 
        wait(0);
    printf("\n-- unzip music berhasil\n");
}

void *decode(void *arg){
    // change directory
    if ((chdir("/home/ariefbadrus/soal-shift-sisop-modul-3-a08-2022/soal1/modul/quote")) < 0) {
        exit(EXIT_FAILURE);
    }
    printf("\n-- change directory to ../modul/quote\n");

    // mengelola file quote.txt
    DIR *folder;
    struct dirent *ep;

    FILE *quote, *reader;

    folder = opendir(".");

    quote = fopen("/home/ariefbadrus/soal-shift-sisop-modul-3-a08-2022/soal1/modul/quote.txt", "a+");

    if(folder != NULL) {
        while(ep = readdir(folder)) {
            char code[256];
            char in[256];
            char *out;
            size_t out_len;

            if(strstr(ep->d_name, "q")){
                // Mendapatkan code
                reader = fopen(ep->d_name, "r");
                if(fgets(code, 256, reader) != NULL) {
                    strcpy(in, code);
                }

                // Decode
                out_len = b64_decodedSize(in) + 1;
                out = malloc(out_len);

                if(!b64_decode(in, (unsigned char *) out, out_len)) {
                    printf("Decode Failure\n");
                    return NULL;
                }
                out[out_len] = '\0';

                // menyimpan hasil decode ke file quote.txt
                fprintf(quote, "%s\n", out);

                free(out);
            }

            printf("-- decode quote ke quote.txt berhasil\n");

        }
        fclose(reader);
        fclose(quote);
    }
    closedir(folder);

    // change directory
    if ((chdir("/home/ariefbadrus/soal-shift-sisop-modul-3-a08-2022/soal1/modul/music")) < 0) {
        exit(EXIT_FAILURE);
    }
    printf("\n-- change directory to ../modul/music\n");

    // mengelola file music.txt
    FILE *music;

    folder = opendir(".");

    music = fopen("/home/ariefbadrus/soal-shift-sisop-modul-3-a08-2022/soal1/modul/music.txt", "a+");

    if(folder != NULL) {
        while(ep = readdir(folder)) {
            char code[256];
            char in[256];
            char *out;
            size_t out_len;

            if(strstr(ep->d_name, "m")){
                // Mendapatkan code
                reader = fopen(ep->d_name, "r");
                if(fgets(code, 256, reader) != NULL) {
                    strcpy(in, code);
                }

                // Decode
                out_len = b64_decodedSize(in) + 1;
                out = malloc(out_len);

                if(!b64_decode(in, (unsigned char *) out, out_len)) {
                    printf("Decode Failure\n");
                    return NULL;
                }
                out[out_len] = '\0';

                // menyimpan hasil decode ke file music.txt
                fprintf(music, "%s\n", out);

                free(out);

            }

            printf("-- decode music ke music.txt berhasil\n");

        }
        fclose(reader);
        fclose(music);
    }
    closedir(folder);
}

void *unzipHasil(void *arg){
    pid_t child = fork();
    int status;
    if (child < 0)
        exit(EXIT_FAILURE);

    else if (child == 0) {
        // unzip hasil.zip
        execl("/bin/unzip", "unzipHasil", "-P", "mihinomenestarief", "/home/ariefbadrus/soal-shift-sisop-modul-3-a08-2022/soal1/modul/hasil.zip", NULL);
    }
    else {
        while((wait(&status)) > 0);
        printf("\n-- unzip hasil berhasil\n");

        pid_t child1 = fork();
        if (child1 < 0)
            exit(EXIT_FAILURE);

        else if (child1 == 0) {
            // menghapus folder hasil
            char * argv[] = {"rm", "-r", "hasil.zip", NULL};
            execv("/bin/rm", argv);
        }
        else {
            printf("\n-- menghapus hasil.zip lama berhasil\n");
        }
    } 
}

void *makeNoTxt(void *arg){
    pid_t child = fork();
    int status;
    if (child < 0)
        exit(EXIT_FAILURE);

    else if (child == 0) {
        sleep(1);
        execl("/bin/touch", "makeNoTxt", "hasil/no.txt", NULL);
    } else {
        printf("\n-- membuat file no.txt\n");
        wait(0);
    }

}